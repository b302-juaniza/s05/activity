--Create an activity.txt inside s05 project and create the SQL code for the following:

    --Return the customerName of the customers who are from the Philippines

    Select customerName from customers where country = "Philippines";

    --Return the contactLastName and contactFirstName of customers with name "La Rochelle Gifts"

    Select contactLastName, contactFirstName from customers where customerName = "La Rochelle Gifts";

    --Return the product name and MSRP of the product named "The Titanic"

    Select productName, MSRP from products where productName = "The Titanic";

    --Return the first and last name of the employee whose email is "jfirrelli@classicmodelcars.com"

    Select firstName, lastName from employees where email = "jfirrelli@classicmodelcars.com";

    --Return the names of customers who have no registered state

    Select customerName from customers where state is null;

    --Return the first name, last name, email of the employee whose last name is Patterson and first name is Steve

    Select firstName, lastName, email from employees where lastName = "Patterson" and firstName = "Steve";

    --Return customer name, country, and credit limit of customers whose countries are NOT USA and whose credit limits are greater than 3000

    Select customerName, country, creditLimit from customers where country != "USA" and creditLimit > 3000;

    --Return the customer numbers of orders whose comments contain the string 'DHL'

    Select customerNumber from orders where comments like "%DHL%";

    --Return the product lines whose text description mentions the phrase 'state of the art'

    Select productLine, textDescription from productLines where textDescription like "%state of the art%";

    --Return the countries of customers without duplication

    Select Distinct country from customers;

    --Return the statuses of orders without duplication

    Select Distinct status from orders;

    --Return the customer names and countries of customers whose country is USA, France, or Canada

    Select customerName, country from customers where country = "USA" or country = "France" or country = "Canada";

    --Return the first name, last name, and office's city of employees whose offices are in Tokyo

    Select employees.firstName, employees.lastName, offices.city from employees
        Join offices on employees.officeCode = offices.officeCode
        where city = "Tokyo";

    --Return the customer names of customers who were served by the employee named "Leslie Thompson"

    Select customerName from customers 
        Join employees on customers.salesRepEmployeeNumber = employees.employeeNumber
        where employees.firstName = "Leslie" and employees.lastName = "Thompson";

    --Return the product names and customer name of products ordered by "Baane Mini Imports"

    Select products.productName, customers.customerName from products
        Inner Join orderDetails on products.productCode = orderDetails.productCode
        Inner Join orders on orderDetails.orderNumber = orders.orderNumber
        Inner Join customers on orders.customerNumber = customers.customerNumber
        where customers.customerName = "Baane Mini Imports";

    --Return the employees' first names, employees' last names, customers' names, and offices' countries of transactions whose customers and offices are located in the same country

    Select employees.firstName, employees.lastName, customers.customerName, offices.country from employees
        Join offices on employees.officeCode = offices.officeCode
        Inner Join customers on offices.country = customers.country;
        
    --Return the product name and quantity in stock of products that belong to the product line "planes" with stock quantities less than 1000.

    Select productName, quantityInStock from products 
        Join productLines on products.productLine = productLines.productLine
        where productLines.productLine = "Planes";

    --Return the customer's name with a phone number containing "+81".

    Select customerName, phone from customers where phone like "%+81%";